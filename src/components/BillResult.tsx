import styles from "../styles/bill-result.module.css";

type PropTypes = {
  bill: number;
  tipPercentage: number;
  tipPercentageIn: number;
  people: number;
  handleReset: () => void;
};

export default function BillResult({
  bill,
  tipPercentage,
  people,
  tipPercentageIn,
  handleReset,
}: PropTypes) {
  const percentage = tipPercentage || tipPercentageIn;
  const billPerPerson = bill / people;
  const tip = (bill * (percentage / 100)) / people;
  const total = billPerPerson + tip;

  const isNotReady = !bill || !percentage || !people;

  return (
    <div className={styles.resultContainer}>
      <header>
        <div className={styles.resultContent}>
          <div>
            <em className={styles.title}>Tip Amount</em>
            <p className={styles.description}>/ person</p>
          </div>
          <h2 className={styles.result}>
            ${isNotReady ? "0.00" : tip.toFixed(2)}
          </h2>
        </div>

        <div className={styles.resultContent}>
          <div>
            <em className={styles.title}>Total</em>
            <p className={styles.description}>/ person</p>
          </div>
          <h2 className={styles.result}>
            ${isNotReady ? "0.00" : Math.round(total * 100) / 100}
          </h2>
        </div>
      </header>

      <button
        onClick={handleReset}
        className={styles.btn}
        disabled={isNotReady ? true : false}
      >
        Reset
      </button>
    </div>
  );
}
