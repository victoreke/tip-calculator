import { DollarIcon, UserIcon } from "./icons";
import styles from "../styles/bill-form.module.css";
import CustomInput from "./shared/CustomTextInput";
import CustomRadioInput from "./shared/CustomRadioInput";
import { ChangeEvent, useState } from "react";
import { isInputNum } from "../utils/validate-input";
import { BillType } from "../type";

const tipOptions = [5, 10, 15, 25, 50];

export default function BillForm({
  bill,
  setBill,
  tipPercentage,
  setTipPercentage,
  people,
  setPeople,
  tipPercentageIn,
  setTipPercentageIn,
}: BillType) {
  const [error, setError] = useState<boolean>(false);

  function handleBill(e: ChangeEvent<HTMLInputElement>) {
    if (isInputNum(e.target.value)) {
      setBill(e.target.value);
    }
  }

  function handleTipPercentage(e: ChangeEvent<HTMLInputElement>) {
    setTipPercentage(e.target.id);
    setTipPercentageIn("");
  }

  function handleTipPercentageIn(e: ChangeEvent<HTMLInputElement>) {
    if (isInputNum(e.target.value)) {
      setTipPercentageIn(e.target.value);
      setTipPercentage("");
    }
  }

  function handlePeople(e: ChangeEvent<HTMLInputElement>) {
    if (isInputNum(e.target.value)) {
      setPeople(e.target.value);
    }
    setError(+e.target.value === 0 ? true : false);
  }

  return (
    <form className={styles.form}>
      <CustomInput
        name="bill"
        placeholder="0"
        icon={<DollarIcon />}
        value={bill}
        onChange={handleBill}
      />

      <div>
        <p className={styles.label}>Select Tip %</p>
        <div className={styles.inputContainer}>
          {tipOptions.map((tip) => (
            <CustomRadioInput
              key={tip}
              htmlFor={tip.toString()}
              name="percentage"
              id={tip}
              checked={tip.toString() !== tipPercentage ? false : true}
              value={tipPercentage}
              onChange={handleTipPercentage}
            />
          ))}
          <input
            type="text"
            name="percentage"
            placeholder="Custom"
            value={tipPercentageIn}
            onChange={handleTipPercentageIn}
          />
        </div>
      </div>

      <CustomInput
        name="Number of People"
        placeholder="0"
        icon={<UserIcon />}
        value={people}
        onChange={handlePeople}
        error={error}
        errorMessage="Can't be zero"
      />
    </form>
  );
}
