import styles from "./custom-text-input.module.css";
import { titleCase } from "../../../utils/title-case";

type CustomInputType = {
  name: string;
  icon: React.ReactNode;
  type?: string;
  placeholder: string;
  value: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  errorMessage?: string;
  error?: boolean;
};

export default function CustomInput({
  name,
  icon,
  type,
  placeholder,
  value,
  onChange,
  errorMessage,
  error,
}: CustomInputType) {
  return (
    <label htmlFor={name} className={styles.formFields}>
      <div className={styles.labelContainer}>
        <p className={styles.label}>{titleCase(name)}</p>
        {errorMessage && error && (
          <span className={styles.error}>{errorMessage}</span>
        )}
      </div>
      <div className={`${styles.inputContainer} ${styles.error}`}>
        <span className={styles.icon}>{icon}</span>
        <input
          type={type ?? "text"}
          name={name}
          id={name}
          placeholder={placeholder}
          onChange={onChange}
          value={value}
          style={
            errorMessage && error ? { borderColor: "hsl(9, 46%, 55%)" } : {}
          }
        />
      </div>
    </label>
  );
}
