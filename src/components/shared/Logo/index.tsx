import styles from "./logo.module.css";

export function Logo() {
  return <h1 className={styles.logo}>Splitter</h1>;
}
