import styles from "./custom-radio-input.module.css";
import { RadioInputType } from "../../../type";

export default function CustomRadioInput({
  name,
  id,
  htmlFor,
  value,
  onChange,
  checked,
}: RadioInputType) {
  return (
    <div className={styles.radioContainer}>
      <input
        type="radio"
        name={name}
        id={id.toString()}
        value={value}
        onChange={onChange}
        className={styles.input}
        checked={checked}
      />
      <label htmlFor={htmlFor} className={styles.label}>
        {id.toString()}%
      </label>
    </div>
  );
}
