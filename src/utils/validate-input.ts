export function isInputNum(input: string | number) {
  if (input === "" || /^[0-9.]*$/.test(input.toString())) return true;
  return false;
}
