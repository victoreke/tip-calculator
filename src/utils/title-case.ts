export const titleCase = (text: string) =>
  text.replace(/\w/, (value) => value.toUpperCase());
