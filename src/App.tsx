import { useState } from "react";
import BillForm from "./components/BillForm";
import BillResult from "./components/BillResult";
import { Logo } from "./components/shared/Logo";
import styles from "./styles/app.module.css";

function App() {
  const [bill, setBill] = useState<string>("");
  const [tipPercentage, setTipPercentage] = useState<string>("");
  const [tipPercentageIn, setTipPercentageIn] = useState<string>("");
  const [people, setPeople] = useState<string>("");

  function reset() {
    setBill("");
    setTipPercentage("");
    setTipPercentageIn("");
    setPeople("");
  }

  return (
    <main className={styles.container}>
      <Logo />
      <div className={styles.formContainer}>
        <BillForm
          bill={bill}
          setBill={setBill}
          tipPercentage={tipPercentage}
          setTipPercentage={setTipPercentage}
          people={people}
          setPeople={setPeople}
          tipPercentageIn={tipPercentageIn}
          setTipPercentageIn={setTipPercentageIn}
        />
        <BillResult
          bill={+bill}
          tipPercentage={+tipPercentage}
          tipPercentageIn={+tipPercentageIn}
          people={+people}
          handleReset={reset}
        />
      </div>
    </main>
  );
}

console.log(import.meta.env);

export default App;
