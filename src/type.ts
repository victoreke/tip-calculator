import { Dispatch, SetStateAction } from "react";

export type BillType = {
  bill: string;
  setBill: Dispatch<SetStateAction<string>>;
  tipPercentage: string;
  setTipPercentage: Dispatch<SetStateAction<string>>;
  people: string;
  setPeople: Dispatch<SetStateAction<string>>;
  tipPercentageIn: string;
  setTipPercentageIn: Dispatch<SetStateAction<string>>;
};

export type RadioInputType = {
  name: string;
  id: string | number;
  htmlFor: string;
  value?: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  checked?: boolean;
  ref?: React.LegacyRef<HTMLInputElement>;
};
