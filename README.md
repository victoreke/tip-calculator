# Frontend Mentor - Tip calculator app

![Design preview for the Tip calculator app coding challenge](./design/desktop-preview.jpg)

## Welcome! 👋

Thanks for checking out this front-end coding challenge.

## How it works

This app is a tip calculator with the following functionalities:

- A bill amount is provided, for example: $142.55
- A tip percentage is selected from the options: `[5, 10, 15, 25, 50, custom]` with a custom input to add a specific percentage value. For example: `15`
- It then takes the number of people to split the bill between, for example: `5`

Given the above parameters, here's the calculation for the tip calculator:

```js
const bill = $125
const percentage = 15%
const noPeopleSplitBill = 5

// Result
const tip = bill x percentage / 100 = 4.27
const billPerPerson = bill / people = 28.51
const totalBillPerPersonPlusTip = billPerPerson + tip = 32.79
```
